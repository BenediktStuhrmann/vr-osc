﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OSCSystem;

public class AppManager : MonoBehaviour {

    public GameObject jointPrefab;
    public GameObject character;
    public string addressPrefix = "/skeleton/";

    public ArrayList jointNames = new ArrayList();
   
    // Use this for initialization
    void Start () {

    }
	
	// Update is called once per frame
	void Update () {

    }

    void OnOscMessage(OSCMessageEventArgs args)
    {
        string jointName = args.Address.Replace(this.addressPrefix, "");

        // if joint already exists, return
        if (this.jointNames.Contains(jointName)) return;

        // otherwise add it to the existing ones and create a new joint as parent of character
        this.jointNames.Add(jointName);

        Vector3 position = args.ExtractVector3(2, 3, 1);

        GameObject jointGo = GameObject.Instantiate(this.jointPrefab, position, Quaternion.identity) as GameObject;
        jointGo.transform.parent = this.character.transform;
        SkeletalJoint joint = jointGo.GetComponent<SkeletalJoint>();
        joint.name = jointName;
    }
}
