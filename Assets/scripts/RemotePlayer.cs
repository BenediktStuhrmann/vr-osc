﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using OSCSystem;

public class RemotePlayer : MonoBehaviour {

    
    public int timeout = 60;         // 60 seconds timeout before destroying
    int lastUpdate = 0;

	public int messageTimeout = 5;
	public Text messageText;

	public Text label;

	public OSCSender sender;
    public OSCReceiver receiver;

    private string playerName;

	// Use this for initialization
	void Start () {
		if( label != null)	
			label.text = name;

        // Save player name
        this.playerName = this.gameObject.name;

        // Configure OSC-Sender
        this.sender = GetComponent<OSCSender>();
        OSCDestination destination = new OSCDestination(ApplicationManager.Instance.serverAddress, ApplicationManager.Instance.serverPort);
        this.sender.AddDestination(destination);

        // Configure OSC-Receiver
        this.receiver = GetComponent<OSCReceiver>();
        this.receiver.AddAddress("/player/" + this.playerName + "/pose"); // receive position
        this.receiver.AddAddress("/player/" + this.playerName + "/message"); // receive messages

        StartCoroutine(Timeout());

	}

    // Function for processing of OSC-Messages
    void OnOscMessage(OSCMessageEventArgs args)
    {
        string address = args.Address.Replace(this.playerName + "/", ""); // remove dynamic player name from address

        switch (address)
        {
            case "/player/pose":
                {
                    // extract and apply received position and rotation
                    Vector3 newPosition = args.ExtractVector3(0, 1, 2);
                    Quaternion newRotation = args.ExtractQuaternion(4, 5, 6, 3);
                    this.transform.SetPositionAndRotation(newPosition, newRotation);
                    
                    UpdateTime();
                    break;
                }
            case "/player/message":
                {
                    if (args.Data[0] != null && args.Data[0].GetType() == typeof(string)) {
                        StopCoroutine("ShowMessage");
                        StartCoroutine("ShowMessage", args.Data[0]);
                    }
                    break;
                }
        }
    }

    private void UpdateTime()
    {
        lastUpdate = 0;
    }

    IEnumerator Timeout()
    {
        while (true)
        {
            yield return new WaitForSeconds(1);

            lastUpdate++;
            if (lastUpdate > timeout)
                Destroy(this.gameObject);
        }


    }

	IEnumerator ShowMessage( string message)
	{
		if (messageText != null) {
			messageText.text = message;
            yield return new WaitForSeconds (5);
			messageText.text = "";
		}
	}
   
}

