﻿ using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class Player : MonoBehaviour {

    private OSCSender sender;
    private string playerName;
   
    
    void Awake()
    {
        GetComponent<Rigidbody>().position = Vector3.ProjectOnPlane(Random.insideUnitSphere * 5.0f, Vector3.up);
    }
	// Use this for initialization
	void Start () {
        // Save player name
        this.playerName = this.gameObject.name;

        // Configure OSCSender
        this.sender = GetComponent<OSCSender>();
        OSCDestination destination = new OSCDestination(ApplicationManager.Instance.serverAddress, ApplicationManager.Instance.serverPort); 
        this.sender.AddDestination(destination);

        // Register on server 
        this.sender.Send("/system/addPlayer", this.playerName, ApplicationManager.Instance.ipAddress);

        StartCoroutine(SendUpdate());
        
    }
	
	// Update is called once per frame
	IEnumerator SendUpdate () {
	    while (true)
        {
            // Send position and rotation over OSC
            float[] values = { this.transform.position.x
                             , this.transform.position.y
                             , this.transform.position.z
                             , this.transform.rotation.w
                             , this.transform.rotation.x
                             , this.transform.rotation.y
                             , this.transform.rotation.z
                             };
            this.sender.Send("/player/" + this.playerName + "/pose", values);

            yield return new WaitForSeconds(ApplicationManager.Instance.updateRate);
        }
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.Return)) 
		{
            // Send message over OSC
            string message = "Hallo!";
            this.sender.Send("/player/" + this.playerName + "/message", message);
        }
	}


   

    void OnApplicationQuit()
    {
        // Unregister from server
        this.sender.Send("/system/removePlayer", this.playerName);
    }
		
}
