﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(OSCSender))]
public class SendTranslation : MonoBehaviour {
	
	OSCSender sender;
	Vector3 lastPosition;
	
	// Use this for initialization
	void Start () {
		sender = GetComponent<OSCSender>();
		lastPosition = this.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(this.transform.position);
		Vector3 translation =  this.transform.position - lastPosition;
		sender.Send("/translate",translation.x, translation.y, translation.z);
		lastPosition = this.transform.position;
	}
}
