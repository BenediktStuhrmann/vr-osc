﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(OSCReceiver))]
public class ApplicationManager : MonoBehaviour {

    public GameObject remotePlayerPrefab;
    public GameObject player;
    public string ipAddress;
    public string serverAddress;
    public int serverPort;
    public float updateRate = 0.1f;

    // Use this for initialization
    void Start () {

       
        this.GetComponent<OSCReceiver>().OnMessage += OnOscMessage;
        
        player = GameObject.FindGameObjectWithTag("Player");

    }

    void Awake()
    {
        ipAddress = Network.player.ipAddress;

        if (string.IsNullOrEmpty(serverAddress))
            serverAddress = "127.0.0.1";
    }

    private void OnOscMessage(OSCSystem.OSCMessageEventArgs args)
    {
        switch (args.Address)
        {
            case "/system/addPlayer":
                {
                    Debug.Log("/addPlayer " + (string)(args.Data[0]));
                    if (remotePlayerPrefab == null) // || player == null || player.name == (string)(args.Data[0]))
                        return;
                    if (GameObject.Find((string)(args.Data[0])) != null)
                        return;
                    GameObject go = GameObject.Instantiate(remotePlayerPrefab, Vector3.zero, Quaternion.identity) as GameObject;
                    RemotePlayer newPlayer = go.GetComponent<RemotePlayer>();
                    newPlayer.name = (string)(args.Data[0]);
                    break;
                }
            case "/system/removePlayer":
                {
                    Debug.Log("/removePlayer " + (string)(args.Data[0]));
                    GameObject remotePlayer = GameObject.Find((string)(args.Data[0]));
                    if (remotePlayer == null)
                        return;
                    Destroy(remotePlayer);
                   
                    break;
                }
        }
    }

    // Update is called once per frame
    void OnApplicationQuit()
    {
       
    }

    private static ApplicationManager instance = null;
    public static ApplicationManager Instance
    {
        get
        {
            if (null == (object)instance)
            {

                instance = FindObjectOfType(typeof(ApplicationManager)) as ApplicationManager;

                if (null == (object)instance)
                {
                    var go = new GameObject("ApplicationManager");
                    //go.hideFlags = HideFlags.HideAndDontSave;
                    //go.hideFlags = HideFlags.HideInHierarchy;
                    instance = go.AddComponent<ApplicationManager>();

                }
            }
            return instance;
        }
    }
}
