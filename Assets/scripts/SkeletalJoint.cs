﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using OSCSystem;

public class SkeletalJoint : MonoBehaviour {

    private OSCReceiver receiver;
    public string addressPrefix = "/skeleton/";
    private string jointName;
    private string myAddress;

	// Use this for initialization
	void Start () {
        this.jointName = this.gameObject.name;
        this.receiver = GetComponent<OSCReceiver>();

        this.myAddress = this.addressPrefix + this.jointName;
        this.receiver.AddAddress(this.myAddress);
        this.receiver.OnMessage += OnOscMessage;
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void Awake()
    {

    }

    void OnOscMessage(OSCMessageEventArgs args)
    {
        if (args.Address != this.myAddress) return;

        // extract and apply received position
        Vector3 position = args.ExtractVector3(2, 3, 1);
        this.transform.localPosition = position;
    }
}