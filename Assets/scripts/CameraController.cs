﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    public GameObject target;
    public float speed = 2f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (target == null) return;

        this.transform.Translate(Vector3.right * this.speed * Time.deltaTime);
        this.transform.LookAt(this.target.transform.position);

        //this.transform.RotateAround(this.target.transform.position, Vector3.up, this.speed * Time.deltaTime);
    }
}
