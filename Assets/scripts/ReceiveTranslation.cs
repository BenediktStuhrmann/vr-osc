﻿using UnityEngine;
using System.Collections;
using OSCSystem;

public class ReceiveTranslation : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnOscMessage(OSCMessageEventArgs args)
	{
		this.transform.Translate(args.ExtractVector3(0,1,2));
	}
}
