using UnityEngine;	
using OSCSystem;

	public static class OSCUnityHelper
	{
		public static Vector3 ExtractVector3(this OSCMessageEventArgs args, int ix, int iy, int iz)
		{
			try
			{
				return new Vector3((float)args.Data[ix],(float)args.Data[iy],(float)args.Data[iz]);
			}
			catch(System.Exception e)
			{
				Debug.Log("extractVector3 failed:" + e.Message);
			}
			return Vector3.zero;
		}
		public static Quaternion ExtractQuaternion(this OSCMessageEventArgs args, int ix, int iy, int iz, int iw)
		{
			try
			{
				return new Quaternion((float)args.Data[ix],(float)args.Data[iy],(float)args.Data[iz],(float)args.Data[iw]);
			}
			catch(System.Exception e)
			{
				Debug.Log("extractQuaternion failed:" + e.Message);
			}
			return Quaternion.identity;
		}
	}

