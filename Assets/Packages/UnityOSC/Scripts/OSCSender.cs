using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using OSCSystem;
using System.Net;

public class OSCSender : MonoBehaviour {
	
	OSCTransmitter oscTransmitter;
	public List<OSCDestination> destinations;
	//public OscDestination[] destinations;
	
	// Use this for initialization
	public void Awake () {
		if(destinations == null)
			destinations = new List<OSCDestination>();
		oscTransmitter = new OSCTransmitter();
		foreach(OSCDestination dest in destinations )
			oscTransmitter.AddDestination(dest.hostAddress, dest.port, dest.transportType);
		//Debug.Log("OSCSender : Awake");
	}

	
	public void Send(OSCData args)
	{
		
		if(oscTransmitter != null)
			oscTransmitter.Send(args.Address, args.Data);
	}
//	
//	public void Send(string address,TransportType transportType, object[] data)
//	{
//		if(oscTransmitter != null)
//			oscTransmitter.Send(address, data, transportType);
//	}
	
	public void Send(string address, params object[] data)
	{
		if(oscTransmitter != null)
			oscTransmitter.Send(address, data);
	}
//	public void Send(string address,params object[] data)
//	{
//		if(oscTransmitter != null)
//			oscTransmitter.Send(address,TransportType.Udp, data);
//	}
	
	public void AddDestination(OSCDestination dest)
	{
		if(!destinations.Contains(dest))
		{
			destinations.Add(dest);
			oscTransmitter.AddDestination(dest.hostAddress, dest.port, dest.transportType);
		}
	}
	public void RemoveDestination(OSCDestination dest)
	{
		if(	destinations.Remove(dest) )
			oscTransmitter.RemoveDestination(dest.hostAddress, dest.port, dest.transportType);
			
		
	}
	
	public void RemoveAllDestinations()
	{
		destinations.Clear();
		oscTransmitter.ClearDestinations();
	}
	
}

public class OSCData
{
	public string Address {	get; set;}
	public object[] Data {	get; set;}
	public TransportType TransportType {	get; set;}
	
	public OSCData(string address, object[] data) : this(address, data, TransportType.Connectionless)
	{}
	
	public OSCData(string address, object[] data, TransportType transportType)
	{
		this.Address = address;
		this.Data = data;
		this.TransportType = transportType;
	}
}
