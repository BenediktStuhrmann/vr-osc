using UnityEngine;
using System.Collections.Generic;
using OSCSystem;

//[ExecuteInEditMode]
public class OSCReceiver : MonoBehaviour {
	
	public int port = 7000;
	public List<string> oscAddressFilter;
	public bool sendUpwards = false;
	
	//public bool useEvent = true;
	public string sendToFunction = "OnOscMessage"; 	// empty = use only events
	private OSCListener oscListener;
	
	public delegate void OnMessageHandler(OSCMessageEventArgs args);
    public event OnMessageHandler OnMessage;
	
	
	
	// Use this for initialization
	public void Awake () 
	{
		
		if(oscAddressFilter == null)
			oscAddressFilter = new List<string>();
		if(oscListener == null)
		{
			oscListener = new OSCListener(port);
			oscListener.OnMessage += OnOscMessageReceived;
		}
		foreach(string oscAddress in oscAddressFilter)
		{
			oscListener.AddAddress(oscAddress);
		}
		
		//Debug.Log("OSCReceiver : Awake");
	}
	
	
	
	
	public void AddAddress(string address)
	{
		oscAddressFilter.Add(address);
		if(oscListener != null)
			oscListener.AddAddress(address);
			
	}
	
	public void RemoveAddress(string address)
	{
		oscAddressFilter.Remove(address);
		if(oscListener != null)
			oscListener.RemoveAddress(address);
			
	}
	
	public void AddAddresses(IEnumerable<string> addresses)
	{
		foreach(string address in addresses)
		{
			oscAddressFilter.Add(address);
			if(oscListener != null)
				oscListener.AddAddress(address);
		}
			
	}
	
	public void RemoveAddresses(IEnumerable<string> addresses)
	{
		foreach(string address in addresses)
		{
			oscAddressFilter.Remove(address);
			if(oscListener != null)
				oscListener.RemoveAddress(address);
		}
			
	}
	public void RemoveAllAddresses()
	{
		foreach(string address in oscAddressFilter)
		{
			
			if(oscListener != null)
				oscListener.RemoveAddress(address);
		}
		oscAddressFilter.Clear();
			
	}
	
	public int Port
	{
		get { return port;}
		set 
		{ 
			
			port = value;
			if(oscListener != null)
				oscListener.Port = port;
		}
	}

	void OnOscMessageReceived(object sender, OSCMessageEventArgs args)
	{
       
        //if(args.Address=="/Body/Joint/HandLeft")
        //Debug.Log(System.DateTime.Now.TimeOfDay.TotalMilliseconds);
        if (OnMessage != null)
		{
			UnityThreadHelper.Dispatcher.Dispatch(() => OnMessage(args));
			
		}
		if(string.IsNullOrEmpty(sendToFunction))
			return;
		if(sendUpwards)
			UnityThreadHelper.Dispatcher.Dispatch(() => SendMessageUpwards(sendToFunction, args, SendMessageOptions.DontRequireReceiver));
		else
			UnityThreadHelper.Dispatcher.Dispatch(() => SendMessage(sendToFunction, args, SendMessageOptions.DontRequireReceiver));


		
	}
	
	void OnDestroy()
	{
       
        if (oscListener != null)
		    oscListener.Dispose();
       
//		
		
	}
	
	
//	void OnApplicationQuit()
//	{
//		if(oscListener != null)
//			oscListener.OnMessage -= OnOscMessageReceived;
//		
//	}
	
}
