using UnityEngine;
using System.Collections;

using System.Collections.Generic;
using OSCSystem;

using System.Net;
using Bespoke.Common.Net;

//[ExecuteInEditMode]
public class OSCServerHelper : MonoBehaviour {
	
	private static OSCServerHelper instance = null;
    public static OSCServerHelper Instance
    {
        get
        {
            if (null == (object)instance)
            {
				
                instance = FindObjectOfType(typeof(OSCServerHelper)) as OSCServerHelper;
				
                if (null == (object)instance)
                {
                    var go = new GameObject("OSCServerManager");
                    //go.hideFlags = HideFlags.HideAndDontSave;
					//go.hideFlags = HideFlags.HideInHierarchy;
                    instance = go.AddComponent<OSCServerHelper>();
                    
                }
            }
            return instance;
        }
    }
	
	
	
	public List<OSCServerConfigUnity> serverConfig = new List<OSCServerConfigUnity> { new OSCServerConfigUnity()};
	

	// Use this for initialization
	public void Start () 
	{
		UnityThreadHelper.EnsureHelper();
		
		
		
		StartOSC();
		//StartCoroutine("ObserveInit");
		
		Debug.Log("OSCServerHelper : Start");
	}
	
	public void StartOSC()
	{

		
		Debug.Log("Starting OSCServer");
		
		foreach(OSCServerConfigUnity configUnity in serverConfig)
		{
			OSCManager.Instance.AddServer(configUnity.Convert());
		}
		
		OSCManager.Instance.Start();
	}
		
	public void StopOSC()
	{
		
		OSCManager.Instance.Stop();
		OSCManager.Instance.ClearServers();
		OSCManager.Instance.UnregisterAllListener();
	}

	
	public void AddConfig(OSCServerConfigUnity config)
	{
		//System.Array.Resize(ref serverConfig, serverConfig.Length + 1);
		if(!serverConfig.Contains(config))

		{
			serverConfig.Add(config);
			OSCManager.Instance.AddServer(config.Convert());
		}
		
		
	}
	
	public void RemoveConfig(OSCServerConfigUnity config)
	{
		OSCManager.Instance.RemoveServer(config.port);
		
		//System.Array.Resize(ref serverConfig, serverConfig.Length + 1);
		serverConfig.Remove(config);
	}
	
	
	
	void OnDestroy()
	{
		Debug.Log("OSCServerHelper: OnDestroy" );

        StopOSC();
        //OSCManager.Instance.Stop();
		//OSCManager.Instance.ClearServers();
	}
}

[System.Serializable()]
public class OSCServerConfigUnity  : System.IEquatable<OSCServerConfigUnity>
{
	public int port = 7000;
	public TransportType transportType = TransportType.Connectionless;
	public TransmissionType transmissionType = TransmissionType.Unicast;
	public string serverAddress = IPAddress.Any.ToString();
	public bool filterMethods;
	
	public OSCServerConfigUnity()
	{
		port = 7000;
		transportType = TransportType.Connectionless;
		transmissionType = TransmissionType.Unicast;
		serverAddress = IPAddress.Any.ToString();
		filterMethods = true;
	}
	
	public OSCServerConfig Convert()
	{
		OSCServerConfig serverConfig = new OSCServerConfig();
		serverConfig.Port = this.port;
		serverConfig.TransmissionType = this.transmissionType;
		serverConfig.TransportType = this.transportType;
		serverConfig.IpAddress = IPAddress.Parse(this.serverAddress);
		serverConfig.FilterMethods = filterMethods;
		return serverConfig;
	}
	
	public bool Equals(OSCServerConfigUnity other) 
	{
	  if (other == null) 
	     return false;
	
	  if (port == other.port & 
			serverAddress == other.serverAddress & 
			transportType == other.transportType &
			transmissionType == other.transmissionType 
			
			)
	     return true;
	  else
	     return false;
	}
}

[System.Serializable()]
public class OSCDestination : System.IEquatable<OSCDestination>
{
	public string hostAddress ;
	public int port ;
	public TransportType transportType;
	
	public OSCDestination()
	{
		hostAddress = "127.0.0.1";
		port = 7000;
		transportType = TransportType.Udp;
	}
	
	public OSCDestination(string hostAddress, int port, TransportType transportType = TransportType.Udp)
	{
		this.hostAddress = hostAddress;
		this.port = port;
		this.transportType = transportType;
	}
	public bool Equals(OSCDestination other) 
	{
	  if (other == null) 
	     return false;
	
	  if (hostAddress == other.hostAddress & port == other.port
							& transportType == other.transportType)
	     return true;
	  else
	     return false;
	}
	
//	public override bool Equals(Object obj)
//	{
//	  if (obj == null) 
//	     return false;
//	
//	  OSCDestination destObj = obj as OSCDestination;
//	  if (destObj == null)
//	     return false;
//	  else   
//	     return Equals(destObj);   
//	}   

	
}

public class OSCDestinationSameValues : EqualityComparer<OSCDestination>
{
	public override bool Equals(OSCDestination b1, OSCDestination b2)
	{
		if (b1.hostAddress == b2.hostAddress & b1.port == b2.port
							& b1.transportType == b2.transportType)
		{
			return true;
		}
		else { return false; }
	}

	public override int GetHashCode(OSCDestination bx)
	{
		int hCode = bx.hostAddress.GetHashCode() ^ bx.port ^ bx.transportType.GetHashCode();
		return hCode.GetHashCode();
	}
}

//public class OSCDestinationSameAddress : EqualityComparer<OSCDestination>
//{
//	public override bool Equals(OSCDestination b1, OSCDestination b2)
//	{
//		if (b1.hostAddress == b2.hostAddress)
//		{
//			return true;
//		}
//		else { return false; }
//	}
//
//	public override int GetHashCode(OSCDestination bx)
//	{
//		int hCode = bx.hostAddress.GetHashCode() ^ bx.port ^ bx.transportType.GetHashCode();
//		return hCode.GetHashCode();
//	}
//}